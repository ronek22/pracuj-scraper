# -*- coding: utf-8 -*-
import scrapy
import json
from ..items import Job
from datetime import datetime
import logging


class JobsSpider(scrapy.Spider):
    name = 'jobs'
    allowed_domains = ['pracuj.pl']
    start_urls = ['https://www.pracuj.pl/praca?et=5%2c4%2c3%2c1%2c2%2c6']

    logging.getLogger().addHandler(logging.StreamHandler())

    custom_settings = {
        'FEED_FORMAT': 'csv',
        'FEED_URI': 'results/jobs/Pracuj_' + datetime.now().strftime("%Y%m%d+%H_%M") + '.csv',
        'FEED_EXPORT_ENCODING': 'utf-8',
        'FEED_EXPORT_FIELDS': ["job_title", "employer", "employer_url", "employer_logo","types_of_contract", "work_schedules",
            "employment_level", "city", "publication_date", "expiration_date", "content"],
        'LOG_FILE': f'logs/jobs_log_{datetime.now().strftime("%Y%m%d+%H_%M")}.log',
        'LOG_LEVEL': 'DEBUG',
    }

    def parse(self, response):
        # number_of_pages = int(response.xpath("//a[@class='pagination_trigger']/text()").getall()[-2])
        number_of_pages = int(response.xpath(
            "//span[@class='pagination_label--max']/text()").get()[1:])

        for page in range(1, number_of_pages+1):
            yield scrapy.Request(f"{response.url}&pn={page}", callback=self.parse_page)

    def parse_page(self, response):
        scripts = response.xpath('//script/text()').getall()
        offers = self._parse_script(scripts)

        for offer in offers:

            main_offer = Job(
                job_title=offer.get('jobTitle'),
                employment_level=offer.get('employmentLevel'),
                employer=offer.get('employer'),
                employer_url=offer.get('companyProfileUrl'),
                employer_logo=offer.get('logo'),
                city=None,
                types_of_contract=offer.get('typesOfContract'),
                work_schedules=offer.get('workSchedules'),
                publication_date=offer.get('lastPublicated'),
                expiration_date=offer.get('expirationDate'),
                content = None
            )

            for city in offer.get('offers'):
                main_offer['city'] = city.get('label')
                yield scrapy.Request(f"https://www.pracuj.pl{city.get('offerUrl')}", callback=self.details_offer, cb_kwargs=dict(job=main_offer))

    def details_offer(self, response, job: Job):
        job['content'] = self.get_content(response)
        yield job

    # region Helpers

    def get_content(self, response):
        offer_text = response.xpath('//div[@id="main"]/descendant::text()').getall()
        offer_text = ' '.join(offer_text)
        offer_text = offer_text.replace(';', ':').replace('\n', ' ').replace('  ', ' ')
        return offer_text.strip()

    def _parse_script(self, scripts):
        the_script = next(
            (x for x in scripts if 'window.__INITIAL_STATE__' in x), False)
        start, end = the_script.index('{"offers"'), the_script.index('};')+1
        the_script = the_script[start:end]

        return json.loads(the_script)['offers']
    # endregion
