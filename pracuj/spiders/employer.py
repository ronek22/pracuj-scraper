# -*- coding: utf-8 -*-
import scrapy
from ..csv_reader import read_distinct
from scrapy.selector import Selector
from ..items import Employer
from datetime import datetime
from collections import OrderedDict
import logging


class EmployerSpider(scrapy.Spider):
    name = 'employer'
    allowed_domains = ['pracuj.pl']
    start_urls = []


    custom_settings = {
        'FEED_FORMAT': 'csv',
        'FEED_URI': 'results/employers/PracujEmployers_' + datetime.now().strftime("%Y%m%d+%H_%M") + '.csv',
        'FEED_EXPORT_ENCODING': 'utf-8',
        'FEED_EXPORT_FIELDS': ["name", "logo", "emails", "url", "url_number", "contact", "more_contacts",  "about_us", "interns","recrutation", "benefits", "other"],
        'LOG_FILE': f'logs/employer_log_{datetime.now().strftime("%Y%m%d+%H_%M")}.log',
        'LOG_LEVEL': 'DEBUG',
    }


    def get_contact_data(self, response, local='contact-details'):
        p_address = response.xpath(f'//div[contains(@class,"{local}")]//div[@class="text"]').get()

        if p_address:
            data = Selector(text=p_address).xpath('//descendant::text()').getall()
        else:
            return ''

        return ''.join(x.strip()+'|' for x in data if (len(x.strip())>0 and 'protected' not in x)) 



    def get_about_us(self, response):
        data = response.xpath('//*[@class="ep-component-box"]/descendant::text()').getall()
        return ' '.join(x.strip() for x in data if len(x.strip())>0)


    def get_news_info(self, response):
        # FIXME: Can't do this, JS must enabled
        data = response.xpath('//section[contains(@class, "news-feed")]//div[@class="box-content"]/descendant::text()').getall()
        return ''.join(x.strip() for x in data if len(x.strip())>0)

    def get_add_links(self, response):
        names = response.xpath('//ul[@id="main-nav-list-1"]//a/@title').getall()
        href = response.xpath('//ul[@id="main-nav-list-1"]//a/@href').getall()
        add_links = list(zip(names, href))

        default_links = ['Aktualności', 'Poznaj nas', 'Oferty pracy', 'Galeria', 'Jak wygląda rekrutacja', 'Benefity', 'Staże i praktyki']

        add_links = [(x,y) for x,y in add_links if x not in default_links]
        return add_links

    

    def _parse(self, response):
        links = response.xpath('//ul[@id="main-nav-list-1"]//a/@title').getall()
        yield {'links': '|'.join(links), 'url': response.url}

    def get_emails(self, response):
        emails = response.xpath('//a[@class="__cf_email__"]/@data-cfemail').getall()
        emails = set(self.decodeEmail(email) for email in emails)
        return '|'.join(emails)



    def parse(self, response):
        contact = self.get_contact_data(response)
        more_contacts = self.get_contact_data(response, 'location')
        about_us = self.get_about_us(response) 
        name = response.xpath('//h1[@itemprop="legalName"]/text()').get()
        logo = response.xpath('//div[@class="logo"]//img[@itemprop="logo"]/@src').get()
        emails = self.get_emails(response)


        item = Employer(
            name=name,
            logo=logo,
            url_number = response.request.meta['redirect_urls'],
            url=response.url,
            emails=emails,
            contact=contact,
            more_contacts=more_contacts,
            about_us=about_us
        )


        other = self.get_add_links(response)


        links = {
            'interns': self.get_link(response, "Staże i praktyki"),
            'recrutation': self.get_link(response, "Jak wygląda rekrutacja"),
            'benefits': self.get_link(response, "Benefity"),
        }


        if links['interns']:
            yield scrapy.Request(links['interns'], callback=self.product_interns, cb_kwargs=dict(item=item, links=links, other=other))
        elif links['recrutation']:
            yield scrapy.Request(links['recrutation'], callback=self.product_recrutation, cb_kwargs=dict(item=item, links=links, other=other))
        elif links['benefits']:
            yield scrapy.Request(links['benefits'], callback=self.product_benefits, cb_kwargs=dict(item=item, links=links, other=other))
        elif len(other)>0:
            yield scrapy.Request(f"https://pracodawcy.pracuj.pl{other.pop()[1]}", callback=self.product_extra, cb_kwargs=dict(item=item, links=links, other=other))
        else:
            yield item

    def product_interns(self, response, item, links, other):
        data = response.xpath('//div[@class="ep-description-container"]/descendant::text()').getall() 
        item['interns'] = ' '.join(x.strip() for x in data if len(x.strip())>0)

        if links['recrutation']:
            yield scrapy.Request(links['recrutation'], callback=self.product_recrutation, cb_kwargs=dict(item=item, links=links, other=other))
        elif links['benefits']:
            yield scrapy.Request(links['benefits'], callback=self.product_benefits, cb_kwargs=dict(item=item, links=links, other=other))
        elif len(other)>0:
            yield scrapy.Request(f"https://pracodawcy.pracuj.pl{other.pop()[1]}", callback=self.product_extra, cb_kwargs=dict(item=item, links=links, other=other))
        else:
            yield item

    def product_recrutation(self, response, item, links, other):
        data = response.xpath('//div[@class="ep-recruitment-container"]/descendant::text()').getall()
        item['recrutation'] =  ' '.join(x.strip() for x in data if len(x.strip())>0)

        if links['benefits']:
            yield scrapy.Request(links['benefits'], callback=self.product_benefits, cb_kwargs=dict(item=item, links=links, other=other))
        elif len(other)>0:
            yield scrapy.Request(f"https://pracodawcy.pracuj.pl{other.pop()[1]}", callback=self.product_extra, cb_kwargs=dict(item=item, links=links, other=other))
        else:
            yield item

    def product_benefits(self, response, item, links, other):
        data = response.xpath('//article[@class="benefits"]/descendant::text()').getall()
        item['benefits'] = ' '.join(x.strip() for x in data if len(x.strip())>0)

        if len(other)>0:
            yield scrapy.Request(f"https://pracodawcy.pracuj.pl{other.pop()[1]}", callback=self.product_extra, cb_kwargs=dict(item=item, links=links, other=other))
        else:
            yield item

    def product_extra(self, response, item, links, other):
        data = response.xpath('//*[@class="ep-component-box"]/descendant::text()').getall()
        data = ' '.join(x.strip() for x in data if len(x.strip())>0)
        if 'other' in item:
            data = f"{item['other']} {data}"
        item['other'] = data

        if len(other)>0:
            yield scrapy.Request(f"https://pracodawcy.pracuj.pl{other.pop()[1]}", callback=self.product_extra, cb_kwargs=dict(item=item, links=links, other=other))
        else:
            yield item




    #region Helpers
    def get_link(self, response, title):
        link = response.xpath(f'//a[@title="{title}"]/@href').get()
        if link: link = f"https://pracodawcy.pracuj.pl{link}"
        return link

    def decodeEmail(self, e):
        de = ""
        k = int(e[:2], 16)

        for i in range(2, len(e)-1, 2):
            de += chr(int(e[i:i+2], 16)^k)

        return de

    #endregion

