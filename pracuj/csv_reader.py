import pandas as pd

def read_distinct(result_file):
    data = pd.read_csv(result_file)
    data = data['employer_url'].unique().tolist()
    return [x for x in data if type(x)==str]