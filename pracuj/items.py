# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field



class Job(Item):
    job_title = Field()
    employment_level = Field()
    employer = Field()
    employer_url = Field()
    employer_logo = Field()
    city = Field()
    types_of_contract = Field() # Rodzaj umowy
    work_schedules = Field() # Rodzaj etatu
    publication_date = Field()
    expiration_date = Field()
    content = Field()

    def __repr__(self):
        return repr({'job_title': self['job_title'], 'employer': self['employer'], 'city': self['city']})

class Employer(Item):
    name = Field()
    logo = Field()
    url = Field()
    url_number = Field()
    emails = Field()
    contact = Field()
    more_contacts = Field()
    about_us = Field()
    interns = Field()
    recrutation = Field()
    benefits = Field()
    other = Field()

    def __repr__(self):
        return repr({'name': self['name'], 'contact': self['contact']})