from __future__ import print_function, unicode_literals
import scrapy
import logging
import glob
from datetime import datetime
from scrapy.crawler import CrawlerProcess, CrawlerRunner
from pracuj.spiders.employer import EmployerSpider
from pracuj.spiders.jobs import JobsSpider
from scrapy.utils.project import get_project_settings
from pracuj.csv_reader import read_distinct
from twisted.internet import reactor, defer
from scrapy.utils.log import configure_logging
from PyInquirer import style_from_dict, prompt


def menu():
    print("="*30)
    print("PRACUJ SCRAPER")
    print("="*30)
    print(
    """
        1. Podaj linki do scrapingu recznie.
        2. Pobierz linki z pliku (links.txt) 
        3. Zakończ
    """)

def main_menu():
    menu = {
        'type': 'list',
        'name':'choice',
        'message':'Co chcesz zrobic?',
        'choices': [
            {
                'name': 'Podaj linki do scrapingu recznie.',
                'value': 1
            },
            {
                'name': 'Pobierz linki z pliku (links.txt)',
                'value': 2
            },
            {
                'name': 'Uruchom crawler dla pracodawcow',
                'value': 3
            },
            {
                'name': 'Zakończ',
                'value': 4
            },
        ]
    }
    answer = prompt(menu)
    return answer['choice']

def job_choose():
    menu = {
        'type': 'list',
        'name':'file',
        'message':'Wybierz plik?',
        'choices': sorted(glob.glob('./results/jobs/*.csv'), reverse=True)
    }

    answer = prompt(menu)
    return answer['file']




start_urls = []


while True:
    choose = main_menu()

    if choose == 1:
        print("Podaj linki oddzielone spacją")
        start_urls = input().split()
    elif choose == 2:
        with open('links.txt', 'r') as l:
            start_urls = [x.strip() for x in l.readlines()]
    elif choose == 3:
        configure_logging()
        runner = CrawlerRunner(settings=get_project_settings())

        file = job_choose()
        employers_file  = file.replace('jobs', 'employers').replace('Jobs', 'Employers')
        EmployerSpider.custom_settings['FEED_URI'] = employers_file
        d = runner.crawl(EmployerSpider, start_urls=read_distinct(file))
        d.addBoth(lambda _: reactor.stop())
        reactor.run()
        exit(0)
    elif choose == 4:
        exit(0)
    else:
        print("Nie ma takiej opcji!")
        continue
    break



print(start_urls)

while start_urls:
    print([start_urls.pop(0)])

configure_logging()
runner = CrawlerRunner(settings=get_project_settings())

@defer.inlineCallbacks
def crawl():
    yield runner.crawl(JobsSpider, start_urls=[start_urls.pop(0)])
    yield runner.crawl(EmployerSpider, start_urls=read_distinct(jobs_file))

while start_urls:
    datetime_csv = datetime.now().strftime("%Y%m%d+%H_%M") + '.csv'
    jobs_file = f"results/jobs/Jobs_{datetime_csv}"
    employers_file = f"results/employers/Employers_{datetime_csv}"

    JobsSpider.custom_settings['FEED_URI'] = jobs_file
    EmployerSpider.custom_settings['FEED_URI'] = employers_file
    
    crawl()
    reactor.run()
reactor.stop()